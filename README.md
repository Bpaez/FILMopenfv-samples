# OpenFV Samples

Assuming you already have OpenFV installed or built on your system, in order to build samples:

<pre><code>
$ git clone git@github.com:abhishekbajpayee/openfv-samples.git
$ cd openfv-samples
$ mkdir bin
$ cd bin
$ cmake [-D PYTHON_LIBS=path/to/libpython2.7.so] ..
$ make
</code></pre>

To run refocus example, use the following command:
<pre><code>
$ ./refocus --config-file=../sample-data/sample_config_refractive.cfg -v=4 -dz=2 -save_path="" -dump_stack=TRUE
</code></pre>
