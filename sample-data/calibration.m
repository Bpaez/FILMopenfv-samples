% Define the grid size in mm and find calibration images
% TO DO: make these a flag
squareSize = 5;
imgFolder = fullfile('pinhole_calibration_data', 'cam_data');
imgSets = imageSet(imgFolder, 'recursive');

% Assume that all images are same size, so reducing computation
I = readimage(imgSets(1), 1);
imgSize = [size(I, 1), size(I, 2)];

% Find amount of cameras
%numCams = size(imgSets, 2);
numCams = 3;

%% Calculate projection error, P matrix, and c vectors

% Stores P and c for each camera (need to print later)
pMatrices = cell(1, numCams);

%cVector = cell(1, numCams);

for i = 1:numCams
    imgFileNames = imgSets(i).ImageLocation;
    [imgPoints, boardSize] = detectCheckerboardPoints(imgFileNames);

    worldPoints = generateCheckerboardPoints(boardSize,squareSize);
    
    params = estimateCameraParameters(imgPoints, worldPoints, 'ImageSize', imgSize);
    
    [rotVector, transVector] = extrinsics(imgPoints(:,:,1), worldPoints, params);
    P = cameraMatrix(params, rotVector, transVector);
    pMatrices{i} = P;
    %cVector{i} = ?
    
    %subplot(3,3,i)
    %showReprojectionErrors(params);
    %figure;
    %showExtrinsics(params);
    %drawnow;
end

%% Printing to text file
% set up text file
fileID = fopen('calib_results.txt', 'w');
fprintf(fileID, "%s\n%d\n", "Pinhole Calibration",numCams);

% get directory names
%TODO: make this flag
dirInfo = dir(fullfile('pinhole_calibration_data', 'cam_data'));
folderNames = cell(1, numCams);

dirSize = size(dirInfo, 1);
for i = 3:dirSize
    folderNames{i-2} = dirInfo(i).name;
end

% print results
for i = 1:numCams
    % folder name
    fprintf(fileID, "%s\n", folderNames{i});
    % p matrix
    printMat(pMatrices{i}, fileID);
    %writematrix(pMatrices{i}, 'calib_results.txt', 'Delimiter', ' ');
    % c vector
    %writematrix(cVector{i}, 'calib_results.txt');
end
fprintf(fileID, "%d", 0);

%%
function x = printMat(matrix, fileID)
    for i = 1:size(matrix, 1)
        for j = 1:size(matrix, 2)
            fprintf(fileID, "%3.3d ", matrix(i, j));
        end
        fprintf(fileID, "%s\n", "");
    end
    x = 0;
end