#include <openfv/parse_settings.h>
#include <openfv/calibration.h>

using namespace cv;
using namespace std;

DEFINE_string(path, "../temp/", "Calibration path");
DEFINE_int32(hgrid, 6, "Horizontal grid size");
DEFINE_int32(vgrid, 5, "Horizontal grid size");
DEFINE_double(gridsize, 5, "Physical grid size");
DEFINE_int32(ref, 0, "Refractive flag");
DEFINE_int32(mtiff, 0, "Multipage tiff flag");
DEFINE_int32(mp4, 0, "mp4 video flag");
DEFINE_int32(skip, 1, "Frames to skip");
DEFINE_int32(s, 0, "Starting frame");
DEFINE_int32(e, 26, "Ending frame");
DEFINE_int32(show_corners, 0, "Show detected corners");
DEFINE_int32(v, 0, "logging level");

int main(int argc, char** argv) {

    // Parsing flags
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    FLAGS_logtostderr=1;

    calibration_settings settings;
    //parse_calibration_settings(FLAGS_path, settings, false);
    //multiCamCalibration calibration(settings);
    // Uses dummy mode
    multiCamCalibration calibration(FLAGS_path, Size(FLAGS_hgrid, FLAGS_vgrid), FLAGS_gridsize, FLAGS_ref, 1, FLAGS_mtiff, FLAG_mp4, FLAGS_skip, FLAG_s, FLAG_e, FLAGS_show_corners);
    calibration.run();

    LOG(INFO)<<"DONE!";

    return 1;

}
